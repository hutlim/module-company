(function($){
	/*"use strict";*/
	$.entwine('ss', function($) {
		$('.ss-gridfield .col-buttons .action.gridfield-button-solved').entwine({
			onclick: function(e) {
				var self = this;
				$('#modal').remove();
				var content = '<div id="modal"><div class="field text"><label class="left" for="Remark">' + ss.i18n._t('EnquiryAction.REMARK', 'Remark') + '</label><input id="Remark" class="text" type="text" /></div></div>';
				var actions = {};
				actions[ss.i18n._t('EnquiryAction.SOLVED', 'Solved')] = function() {
	        		var remark = $('#modal').find('#Remark').val();

	          		$(this).dialog("close");

					// If the button is disabled, do nothing.
					if (self.button('option', 'disabled')) {
						e.preventDefault();
						return;
					}
					
					self.getGridField().reload(
						{data: [{name: self.attr('name'), value: self.val()}, {name: 'Remark', value: remark}]},
						function(){
							statusMessage(ss.i18n._t('EnquiryAction.SOLVED_SUCCESS', 'Enquiry has been mark as solved'), 'good');
						}
					);

					e.preventDefault();
	        	};
				$('body').append(content);
				$('#modal').dialog({
					resizable : false,
					modal : true,
					autoOpen : true,
					width: 500,
			      	buttons: actions
				});
			}
		});
	});

}(jQuery));