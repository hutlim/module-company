if ( typeof (ss) == 'undefined' || typeof (ss.i18n) == 'undefined') {
	console.error('Class ss.i18n not defined');
} else {
	ss.i18n.addDictionary('zh', {
		'EnquiryAction.REMARK' : '备注',
		'EnquiryAction.SOLVED' : '已解决',
		'EnquiryAction.SOLVED_SUCCESS' : '查询已被标记为已解决'
	});
}