if ( typeof (ss) == 'undefined' || typeof (ss.i18n) == 'undefined') {
	console.error('Class ss.i18n not defined');
} else {
	ss.i18n.addDictionary('en', {
		'EnquiryAction.REMARK' : 'Remark',
		'EnquiryAction.SOLVED' : 'Solved',
		'EnquiryAction.SOLVED_SUCCESS' : 'Enquiry has been mark as solved'
	});
}