<?php

class Company_SiteConfigExtension extends DataExtension {
    private static $db = array(
        'CompanyName' => 'Varchar(100)',
        'CompanyAddress' => 'Varchar(255)',
        'CompanySuburb' => 'Varchar(64)',
        'CompanyState' => 'Varchar(64)',
        'CompanyPostcode' => 'Varchar(10)',
        'CompanyCountry' => 'Varchar(2)',
        'CompanyGeneralEmail' => 'Varchar(100)',
        'CompanySalesEmail' => 'Varchar(100)',
        'CompanySupportEmail' => 'Varchar(100)',
        'CompanyPhone' => 'Varchar(20)',
        'CompanyFax' => 'Varchar(20)',
        'CompanyWebsite' => 'Varchar(100)'
    );

    private static $has_one = array('CompanyLogo' => 'Image');
	
	function updateFieldLabels(&$labels) {
		$labels['CompanyName'] = _t('Company_SiteConfigExtension.COMPANY_NAME', 'Company Name');
		$labels['CompanyAddress'] = _t('Company_SiteConfigExtension.ADDRESS', 'Address');
		$labels['CompanySuburb'] = _t('Company_SiteConfigExtension.SUBURB', 'Suburb');
		$labels['CompanyState'] = _t('Company_SiteConfigExtension.STATE', 'State');
		$labels['CompanyPostcode'] = _t('Company_SiteConfigExtension.POSTCODE', 'Postcode');
		$labels['CompanyCountry'] = _t('Company_SiteConfigExtension.COUNTRY', 'Country');
		$labels['CompanyGeneralEmail'] = _t('Company_SiteConfigExtension.GENERAL_EMAIL', 'General Email');
		$labels['CompanySalesEmail'] = _t('Company_SiteConfigExtension.SALES_EMAIL', 'Sales Email');
		$labels['CompanySupportEmail'] = _t('Company_SiteConfigExtension.SUPPORT_EMAIL', 'Support Email');
		$labels['CompanyPhone'] = _t('Company_SiteConfigExtension.PHONE', 'Phone');
		$labels['CompanyFax'] = _t('Company_SiteConfigExtension.FAX', 'Fax');
		$labels['CompanyWebsite'] = _t('Company_SiteConfigExtension.WEBSITE', 'Website URL');
		$labels['CompanyLogo'] = _t('Company_SiteConfigExtension.COMPANY_LOGO', 'Company Logo');
    }

    function updateCMSFields(FieldList $fields) {
    	$fields->findOrMakeTab("Root.Company", _t('Company_SiteConfigExtension.COMPANY_DETAILS', 'Company Details'));
        $fields->addFieldToTab("Root.Company", HeaderField::create('AddressHeader', _t('Company_SiteConfigExtension.ADDRESS_HEADER', 'Address')));
        $fields->addFieldToTab("Root.Company", TextField::create('CompanyName', $this->owner->fieldLabel('CompanyName')));
        $fields->addFieldToTab("Root.Company", TextField::create('CompanyAddress', $this->owner->fieldLabel('CompanyAddress')));
        $fields->addFieldToTab("Root.Company", TextField::create('CompanySuburb', $this->owner->fieldLabel('CompanySuburb')));
        $fields->addFieldToTab("Root.Company", TextField::create('CompanyState', $this->owner->fieldLabel('CompanyState')));
        $fields->addFieldToTab("Root.Company", TextField::create('CompanyPostcode', $this->owner->fieldLabel('CompanyPostcode')));
        $fields->addFieldToTab("Root.Company", CountryDropdownField::create('CompanyCountry', $this->owner->fieldLabel('CompanyCountry')));
        $fields->addFieldToTab("Root.Company", UploadField::create('CompanyLogo', $this->owner->fieldLabel('CompanyLogo')));

        $fields->addFieldToTab("Root.Company", HeaderField::create('ContactHeader', _t('Company_SiteConfigExtension.CONTACT_HEADER', 'Contact')));
        $fields->addFieldToTab("Root.Company", EmailField::create('CompanyGeneralEmail', $this->owner->fieldLabel('CompanyGeneralEmail')));
        $fields->addFieldToTab("Root.Company", EmailField::create('CompanySalesEmail', $this->owner->fieldLabel('CompanySalesEmail')));
        $fields->addFieldToTab("Root.Company", EmailField::create('CompanySupportEmail', $this->owner->fieldLabel('CompanySupportEmail')));
        $fields->addFieldToTab("Root.Company", TextField::create('CompanyPhone', $this->owner->fieldLabel('CompanyPhone')));
        $fields->addFieldToTab("Root.Company", TextField::create('CompanyFax', $this->owner->fieldLabel('CompanyFax')));
        $fields->addFieldToTab("Root.Company", TextField::create('CompanyWebsite', $this->owner->fieldLabel('CompanyWebsite')));
    }

    /**
     * @return bool
     */
    public function hasAddress() {
        return ($this->owner->CompanyAddress && $this->owner->CompanySuburb && $this->owner->CompanyState && $this->owner->CompanyPostcode && $this->owner->CompanyCountry);
    }

    /**
     * Returns the full address as a simple string.
     *
     * @return string
     */
    public function getFullAddress() {
        $address = '';
        if($this->owner->CompanyAddress) {
            $address = sprintf('%s, %s', $address, $this->owner->CompanyAddress);
        }
        if($this->owner->CompanySuburb) {
            $address = sprintf('%s, %s', $address, $this->owner->CompanySuburb);
        }
        if($this->owner->CompanyState) {
            $address = sprintf('%s, %s', $address, $this->owner->CompanyState);
        }
        if($this->owner->CompanyPostcode) {
            $address = sprintf('%s, %d', $address, $this->owner->CompanyPostcode);
        }
        if($this->getCompanyCountryName()) {
            $address = sprintf('%s, %s', $address, $this->getCompanyCountryName());
        }
        $address = trim($address, ',');
        return $address;
    }

    /**
     * Returns the full address in a simple HTML template.
     *
     * @return string
     */
    public function getFullAddressHTML() {
        return $this->owner->renderWith('CompanyAddress');
    }

    /**
     * Returns a static google map of the address, linking out to the address.
     *
     * @param  int $width
     * @param  int $height
     * @return string
     */
    public function AddressMap($width, $height) {
        $data = $this->owner->customise(array(
            'Width' => $width,
            'Height' => $height,
            'Address' => rawurlencode($this->getFullAddress())
        ));
        return $data->renderWith('CompanyAddressMap');
    }

    /**
     * Returns the full address in a simple HTML template.
     *
     * @return string
     */
    public function getFullContactHTML() {
        return $this->owner->renderWith('CompanyContact');
    }

    /**
     * Returns the country name (not the 2 character code).
     *
     * @return string
     */
    public function getCompanyCountryName() {
        return Zend_Locale::getTranslation($this->owner->CompanyCountry, "country", i18n::get_locale());
    }

}
