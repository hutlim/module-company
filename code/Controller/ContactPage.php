<?php

class ContactPage_Controller extends Page_Controller {
    private static $allowed_actions = array(
        'Form'
    );
    
    public function Title(){
        return _t('ContactPage.CONTACT_US', 'Contact Us');
    }
    
    public function index(){
        if($this->request->isAjax()){
            return $this->Form()->forTemplate();
        }
        
        return array();
    }
    
    public function Form(){
        return ContactForm::create($this, 'Form');
    }
}
