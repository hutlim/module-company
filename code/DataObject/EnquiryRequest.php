<?php

class EnquiryRequest extends DataObject implements PermissionProvider {
    private static $singular_name = "Enquiry Request";
    private static $plural_name = "Enquiry Requests";
    
    private static $email_to = '';
    private static $email_template = '';
    
    private static $db = array(
        'Type' => "Dropdown('EnquiryRequestTypeList')",
        'Name' => 'Varchar(100)',
        'Email' => 'Varchar(100)',
        'Phone' => 'Varchar(20)',
        'Message' => 'Text',
        'Remark' => 'Text',
        'IsSolved' => 'Boolean'
    );
	
	private static $has_one = array(
		'Member' => 'Member',
		'Admin' => 'Member'
	);
    
    private static $default_sort = "Created DESC";
    
    private static $searchable_fields = array(
        'Type',
        'Created' => array(
			'field' => 'DateField',
			'filter' => 'DateMatchFilter'
		),
		'Member.Username',
        'Name',
        'Email',
        'Phone',
        'Remark',
        'IsSolved',
        'Admin.Username'
    );

    private static $summary_fields = array(
        'Type.Title',
        'Created.Nice',
		'Member.Username',
        'Name',
        'Email',
        'Phone',
        'Message',
        'Remark',
        'IsSolved.Nice',
        'Admin.Username'
    );
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['Created'] = _t('EnquiryRequest.DATE', 'Date');
		$labels['Created.Nice'] = _t('EnquiryRequest.DATE', 'Date');
		$labels['Type'] = _t('EnquiryRequest.TYPE', 'Type');
		$labels['Type.Title'] = _t('EnquiryRequest.TYPE', 'Type');
		$labels['Member'] = _t('EnquiryRequest.USERNAME', 'Username');
		$labels['Member.Username'] = _t('EnquiryRequest.USERNAME', 'Username');
		$labels['Name'] = _t('EnquiryRequest.NAME', 'Name');
		$labels['Email'] = _t('EnquiryRequest.EMAIL', 'Email');
		$labels['Phone'] = _t('EnquiryRequest.PHONE', 'Phone');
		$labels['Message'] = _t('EnquiryRequest.MESSAGE', 'Message');
		$labels['Remark'] = _t('EnquiryRequest.REMARK', 'Remark');
		$labels['IsSolved'] = _t('EnquiryRequest.IS_SOLVED', 'Is Solved?');
		$labels['IsSolved.Nice'] = _t('EnquiryRequest.IS_SOLVED', 'Is Solved?');
		$labels['Admin'] = _t('EnquiryRequest.UPDATED_BY', 'Updated By');
		$labels['Admin.Username'] = _t('EnquiryRequest.UPDATED_BY', 'Updated By');
		
		return $labels;	
	}
    
    function getCMSFields() {
        $fields = parent::getCMSFields();

        if($this->exists()){
            $fields->makeFieldReadonly('Type');
            $fields->makeFieldReadonly('Name');
            $fields->makeFieldReadonly('Email');
            $fields->makeFieldReadonly('Phone');
            $fields->makeFieldReadonly('Message');
			$fields->makeFieldReadonly('Remark');
			$fields->makeFieldReadonly('IsSolved');
			$fields->makeFieldReadonly('MemberID');
			$fields->makeFieldReadonly('AdminID');
        }
        else{
            $fields->insertAfter(UsernameField::create('SetUsername', _t('EnquiryRequest.USERNAME','Username')), 'Type');
            $fields->removeByName('Name');
            $fields->removeByName('Email');
            $fields->removeByName('Phone');
            $fields->removeByName('MemberID');
            $fields->removeByName('AdminID');    
        }

        return $fields;
    }
    
    function getFrontEndFields($params = null) {
        $fields = parent::getFrontendFields($params);
        $fields->dataFieldByName('Type')->setEmptyString(_t('EnquiryRequest.PLEASE_SELECT', 'Please select...'));
		$fields->replaceField('Email', EmailField::create('Email', $this->fieldLabel('Email')));
        $fields->removeByName('Remark');
		$fields->removeByName('IsSolved');
		$fields->removeByName('MemberID');
		$fields->removeByName('AdminID');

        return $fields;
    }

	function onBeforeWrite(){
		parent::onBeforeWrite();
		
        if(!empty($this->SetUsername)){
            $this->MemberID = Distributor::get_id_by_username($this->SetUsername);    
        }
        
		if(!$this->MemberID){
			$this->MemberID = Distributor::currentUserID();
		}
        
        if($this->MemberID){
            if(empty($this->Name)){
                $this->Name = $this->Member()->Name;
            }
            
            if(empty($this->Email)){
                $this->Email = $this->Member()->Email;
            }
            
            if(empty($this->Phone)){
                $this->Phone = $this->Member()->Mobile;
            }
        }

		if($this->isChanged('IsSolved') && $this->IsSolved){
			$this->AdminID = Member::currentUserID();
		}
	}
    
    function send(){
        if($this->Type == 'General Enquiry'){
            $email_to = SiteConfig::current_site_config()->CompanyGeneralEmail ? SiteConfig::current_site_config()->CompanyGeneralEmail : $this->config()->email_to;
            if($email_to){
                $email_from = sprintf("%s <%s>", $this->Name, $this->Email);
                $body = sprintf("<p>You've an enquiry request from user. See below for the details.</p><p>Name: %s</p><p>Email: %s</p><p>Phone: %s</p><p>Message: %s</p>", $this->Name, $this->Email, $this->Phone, $this->Message);
                $e = new Email($email_from, $email_to, 'Enquiry Request', $body);
                $e->populateTemplate($this);
                if($this->config()->email_template){
                    $e->setTemplate($this->config()->email_template);
                }
                $e->send();
            }
        }
        else if($this->Type == 'Support Enquiry'){
            $email_to = SiteConfig::current_site_config()->CompanySupportEmail ? SiteConfig::current_site_config()->CompanySupportEmail : $this->config()->email_to;
            if($email_to){
                $email_from = sprintf("%s <%s>", $this->Name, $this->Email);
                $body = sprintf("<p>You've an enquiry request from user. See below for the details.</p><p>Name: %s</p><p>Email: %s</p><p>Phone: %s</p><p>Message: %s</p>", $this->Name, $this->Email, $this->Phone, $this->Message);
                $e = new Email($email_from, $email_to, 'Enquiry Request', $body);
                $e->populateTemplate($this);
                if($this->config()->email_template){
                    $e->setTemplate($this->config()->email_template);
                }
                $e->send();
            }
        }
    }

	function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_EnquiryRequest');
    }

    function canEdit($member = false) {
    	if(!$this->exists() && $this->canCreate($member)){
    		return true;
    	}
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_EnquiryRequest');
    }

    public function providePermissions() {
        return array(
            'VIEW_EnquiryRequest' => array(
                'name' => _t('EnquiryRequest.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('EnquiryRequest.PERMISSIONS_CATEGORY', 'Enquiry Request')
            ),
            'CREATE_EnquiryRequest' => array(
                'name' => _t('EnquiryRequest.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('EnquiryRequest.PERMISSIONS_CATEGORY', 'Enquiry Request')
            )
        );
    }
}
?>