<?php
class EnquiryRequestTypeList extends DropdownList implements PermissionProvider {
    private static $singular_name = "Enquiry Request Type List";
    private static $plural_name = "Enquiry Request Type Lists";
    
    private static $default_records = array(
        array(
            'Code' => 'General Enquiry',
            'Title' => 'General Enquiry',
            'Sort' => 10,
            'Locale' => 'en_US'
        ),
		array(
            'Code' => 'Support Enquiry',
            'Title' => 'Support Enquiry',
            'Sort' => 20,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'General Enquiry',
            'Title' => '一般查询',
            'Sort' => 30,
            'Locale' => 'zh_CN'
        ),
		array(
            'Code' => 'Support Enquiry',
            'Title' => '系统查询',
            'Sort' => 40,
            'Locale' => 'zh_CN'
        )
    );

    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($kyc = EnquiryRequestTypeList::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $kyc->Title;
        }
		return $code;
    }
}
?>
