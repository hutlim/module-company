<?php
/**
 * @package company
 */
class EnquiryGridFieldDataColumns implements GridField_ColumnProvider, GridField_ActionProvider {
    public function augmentColumns($gridField, &$columns) {
        if(!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }

    public function getColumnAttributes($gridField, $record, $columnName) {
        return array('class' => 'col-buttons');
    }

    public function getColumnMetadata($gridField, $columnName) {
        if($columnName == 'Actions') {
            return array('title' => '');
        }
    }

    public function getColumnsHandled($gridField) {
        return array('Actions');
    }

    public function getColumnContent($gridField, $record, $columnName) {
    	Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
		Requirements::javascript(FRAMEWORK_DIR . '/javascript/i18n.js');
		Requirements::add_i18n_javascript('company/javascript/lang');
		Requirements::javascript('company/javascript/EnquiryAction.min.js');
    	Requirements::css('company/css/EnquiryGridFieldDataColumns.css');

		if(!$record->IsSolved){
			return GridField_FormAction::create($gridField, 'Solved' . $record->ID, false, "solved", array('RecordID' => $record->ID))->addExtraClass('gridfield-button-solved')->setAttribute('title', _t('EnquiryGridFieldDataColumns.BUTTONSOLVED', 'Solved'))->setAttribute('data-icon', 'accept')->setDescription(_t('EnquiryGridFieldDataColumns.IS_SOLVED', 'Is Solved?'))->Field();
		}
    }

    public function getActions($gridField) {
        return array(
            'solved'
        );
    }

    public function handleAction(GridField $gridField, $actionName, $arguments, $data) {
        if($actionName == 'solved') {
            $item = $gridField->getList()->byID($arguments['RecordID']);
            if(!$item) {
                return;
            }
			
			try {
	            DB::getConn()->transactionStart();
				if(isset($data['Remark']) && $data['Remark']){
					$item->Remark = $data['Remark'];
				}
				$item->IsSolved = 1;
				$item->write();
	            DB::getConn()->transactionEnd();
	        }
	        catch(ValidationException $e) {
	            DB::getConn()->transactionRollback();
	            throw new ValidationException($e->getMessage(), 0);
	        }
        }
    }
}
