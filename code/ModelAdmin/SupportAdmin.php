<?php

class SupportAdmin extends GeneralModelAdmin {
    private static $url_segment = 'support';
    private static $menu_title = 'Support';
    private static $menu_icon = 'company/images/support-icon.png';
	private static $managed_models = array(
		'EnquiryRequest'
	);
    
    public function getEditForm($id = null, $fields = null) {
        $list = $this->getList();
        if(ClassInfo::exists('GridFieldExportToExcelButton')){
        	$exportButton = new GridFieldExportToExcelButton('buttons-after-left');
		}
		else{
			$exportButton = new GridFieldExportButton('buttons-after-left');
		}
        $exportButton->setExportColumns($this->getExportFields());
        
		if($this->modelClass == 'EnquiryRequest'){
	        $listField = GridField::create(
	            $this->sanitiseClassName($this->modelClass),
	            false,
	            $list,
	            $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
	                ->removeComponentsByType('GridFieldFilterHeader')
	                ->addComponents(new GridFieldEmailButton(), new EnquiryGridFieldDataColumns(), new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
	        );
		}
		else{
			$listField = GridField::create(
	            $this->sanitiseClassName($this->modelClass),
	            false,
	            $list,
	            $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
	                ->removeComponentsByType('GridFieldFilterHeader')
	                ->addComponents(new GridFieldEmailButton(), new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
	        );
		}

        // Validation
        if(singleton($this->modelClass)->hasMethod('getCMSValidator')) {
            $detailValidator = singleton($this->modelClass)->getCMSValidator();
            $listField->getConfig()->getComponentByType('GridFieldDetailForm')->setValidator($detailValidator);
        }

        $form = CMSForm::create( 
            $this,
            'EditForm',
            new FieldList($listField),
            new FieldList()
        )->setHTMLID('Form_EditForm');
		$form->setResponseNegotiator($this->getResponseNegotiator());
        $form->addExtraClass('cms-edit-form cms-panel-padded center');
        $form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
        $editFormAction = Controller::join_links($this->Link($this->sanitiseClassName($this->modelClass)), 'EditForm');
        $form->setFormAction($editFormAction);
        $form->setAttribute('data-pjax-fragment', 'CurrentForm');

        $this->extend('updateEditForm', $form);
        
        return $form;
    }
}
